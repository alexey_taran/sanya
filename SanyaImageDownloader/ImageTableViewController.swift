//
//  ImageTableViewController.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 28.08.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import UIKit

class ImageTableViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let imageUrls = DemoURL.demoUrls
    private var detailedImageModel: ImageModel?

    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
        if let cell = cell as? ImageCollectionViewCell {
            cell.imageModel = imageUrls[indexPath.row]
        }
        return cell
    }
    
    // MARK: - Collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        detailedImageModel = imageUrls[indexPath.row]
        if detailedImageModel != nil {
            performSegue(withIdentifier: "toDetailPage", sender: collectionView.cellForItem(at: indexPath))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailPage" {
            if detailedImageModel != nil {
                let destination = segue.destination as! DetailPageViewController
                destination.initImageModel = self.detailedImageModel
            }
        }
    }
}
