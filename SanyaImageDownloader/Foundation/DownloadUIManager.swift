//
//  DownloadUIManager.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 29.08.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import UIKit
import CommonCrypto

class DownloadUIManager {
    
    static let shared = DownloadUIManager()
    private let localCache = NSCache<NSString, UIImage>() //URL, UIImage
    private var directory: URL?
    private var runningTasks = [URL: URLSessionTask]()
    
    private init() {
        directory = URL.createFolder(folderName: "Sanya Image Downloader", in: FileManager.SearchPathDirectory.applicationSupportDirectory)
    }
    
    private func isImageSavedToDisk(by url: URL) -> Bool {
        let filename = imageFileName(by: url)
        guard let directory = directory else {
            return false
        }
        let url = URL(fileURLWithPath: directory.absoluteString).appendingPathComponent(filename).path
        return FileManager.default.fileExists(atPath: url)
    }
    
    private func savedImage(with url: URL, _ completion: @escaping (_ url: URL, _ image: UIImage?, _ error: Error?) -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            let filename = self.imageFileName(by: url)
            guard let directory = self.directory else {
                completion(url, nil, NSError.init(domain: "Can't find directory", code: 0, userInfo: nil))
                return
            }
            
            if let image = UIImage(contentsOfFile: URL(fileURLWithPath: directory.absoluteString).appendingPathComponent(filename).path) {
                completion(url, image, nil)
            } else {
                completion(url, nil, NSError.init(domain: "Can't load image", code: 0, userInfo: nil))
            }
        }
    }
    
    private func saveImageToDisk(for image: UIImage, url: URL) {
        DispatchQueue.global(qos: .utility).async {
            guard let data = image.pngData() else {
                print("data is not image")
                return
            }
            guard let directory = self.directory else {
                print("directory is nil")
                return
            }
            do {
                try data.write(to: directory.appendingPathComponent(self.imageFileName(by: url)))
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    
    private func imageFileName(by url: URL) -> String {
        return md5(url.absoluteString).appending(Keys.imageFileExtension)
    }
    
    func downloadImage(from url: URL, completion: @escaping (_ url: URL, _ image: UIImage?, _ error: Error?) -> Void) {
        assert(Thread.isMainThread)
        guard !runningTasks.keys.contains(url) else {
            print("runningTasks contains the url: \(url.absoluteString)")
            return
        }

        //check local cache
        if let image = localCache.object(forKey: url.absoluteString as NSString) {
            completion(url, image, nil)
            return
        }
        
        //check disk cache
        if isImageSavedToDisk(by: url) {
            savedImage(with: url, completion)
            return
        }

        //download image
        let task = DownloadManager.shared.data(by: url) { [weak self] (data, error) in
            if let data = data, let image = UIImage(data: data) {
                self?.localCache.setObject(image, forKey: url.absoluteString as NSString)
                self?.saveImageToDisk(for: image, url: url)
                completion(url, image, nil)
            } else if let error = error {
                completion(url, nil, error)
            }
            self?.runningTasks.removeValue(forKey: url)
        }

        runningTasks[url] = task
    }
    
    private func md5(_ string: String) -> String {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        
        context.deallocate()
        
        // Optimism :)
        return digest.map { String(format:"%02x", $0) }.joined()
    }
}

extension DownloadUIManager {
    
    private struct Keys {
        static let imageFileExtension = ".png"
    }
}

extension URL {
    static func createFolder(folderName: String, in directoryType: FileManager.SearchPathDirectory) -> URL? {
        let fileManager = FileManager.default
        // Get document directory for device, this should succeed
        if let documentDirectory = fileManager.urls(for: directoryType,
                                                    in: .userDomainMask).first {
            // Construct a URL with desired folder name
            let folderURL = documentDirectory.appendingPathComponent(folderName)
            // If folder URL does not exist, create it
            if !fileManager.fileExists(atPath: folderURL.path) {
                do {
                    // Attempt to create folder
                    try fileManager.createDirectory(atPath: folderURL.path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
                } catch {
                    // Creation failed. Print error, return nil
                    print(error.localizedDescription)
                    return nil
                }
            }
            return folderURL
        }
        // Will only be called if document directory not found
        return nil
    }
}
