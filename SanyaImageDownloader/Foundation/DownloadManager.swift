//
//  DownloadManager.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 29.08.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import Foundation

class DownloadManager {
    static public let shared = DownloadManager()
    private var handlers = [URL:URLSessionDataTask]()
    
    private init() {}
    
    func data(by url: URL, completionHandler: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask  {
        if let foundTask = handlers[url] {
            print("found task, returning")
            return foundTask
        }
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            DispatchQueue.main.async {
                if let error = error {
                    //some error happens
                    completionHandler(nil, error)
                    return
                } else if let data = data {
                    completionHandler(data, nil)
                }
            }
            self?.handlers.removeValue(forKey: url)
        }
        handlers[url] = task
        task.resume()
        return task
    }
}
