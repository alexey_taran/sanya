//
//  DemoURL.swift
//  Cassini
//
//  Created by Alexey Taran on 16.07.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import Foundation

struct DemoURL {
    static var demoUrls: [ImageModel] = {
        var urls = [ImageModel]()
        for index in 1...50 {
            urls.append(ImageModel(url: URL(string: "https://picsum.photos/1200/1200?image=\(index)")!,
                                   title: "Image #\(index)", thumbnailUrl: URL(string: "https://picsum.photos/200/200?image=\(index)")!))
        }
        return urls
    }()
    static func getNextImageModel(for imageModel: ImageModel, isNext: Bool) -> ImageModel? {
        for i in demoUrls.indices {
            if demoUrls[i] === imageModel {
                if isNext {
                    return demoUrls.nextOrNil(for: i)
                } else {
                    return demoUrls.previousOrNil(for: i)
                }
            }
        }
        return nil
    }
}
