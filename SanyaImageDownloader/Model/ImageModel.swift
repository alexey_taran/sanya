//
//  ImageModel.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 30.08.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import Foundation

class ImageModel {
    public var thumbnailUrl: URL
    public var imageUrl: URL
    public var title: String
    
    public init(url: URL, title: String, thumbnailUrl: URL) {
        self.imageUrl = url
        self.title = title
        self.thumbnailUrl = thumbnailUrl
    }
}
