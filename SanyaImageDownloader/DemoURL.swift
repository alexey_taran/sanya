//
//  DemoURL.swift
//  Cassini
//
//  Created by Alexey Taran on 16.07.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import Foundation

struct DemoURL {
    static var demoUrls: [ImageModel] = {
        var urls = [ImageModel]()
        for index in 1...40 {
            urls.append(ImageModel(url: URL(string: "https://picsum.photos/400/400?image=\(index)"), title: "Image #\(index)"))
        }
        return urls
    }()
}
