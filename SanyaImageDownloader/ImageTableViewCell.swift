//
//  ImageTableViewCell.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 28.08.2018.
//  Copyright © 2018 Alexey Taran. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var imageModel: ImageModel? {
        didSet {
            self.titleLabel.text = self.imageModel?.title
            startDownload()
        }
    }

    func startDownload() {
        guard let imageUrl = self.imageModel?.thumbnailUrl else {
            imageDownloadError(NSError(domain: "Image URL = nil", code: 0, userInfo: nil))
            return
        }
        self.iconImageView.image = nil
        DownloadUIManager.shared.downloadImage(from: imageUrl) { [weak self] (url, image, error) in
            if url == self?.imageModel?.thumbnailUrl {
                if (image != nil) {
                    self?.iconImageView.image = image
                } else if (error != nil) {
                    self?.imageDownloadError(error!)
                }
            }
        }
    }
    
    func imageDownloadError(_ error: Error) {
        //something went wrong
        iconImageView.image = UIImage(named: "placeholder")
        print(error.localizedDescription)
    }
}
