//
//  ImageCollectionViewCell.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 28.10.2019.
//  Copyright © 2019 Alexey Taran. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    var imageModel: ImageModel? {
        didSet {
            startDownload()
        }
    }
    
    func startDownload() {
        guard let imageUrl = self.imageModel?.thumbnailUrl else {
            imageDownloadError(NSError(domain: "Image URL = nil", code: 0, userInfo: nil))
            return
        }
        self.imageView.image = UIImage(named: "placeholder")
        DownloadUIManager.shared.downloadImage(from: imageUrl) { [weak self] (url, image, error) in
            if url == self?.imageModel?.thumbnailUrl {
                if (image != nil) {
                    self?.imageView.image = image
                } else if (error != nil) {
                    self?.imageDownloadError(error!)
                }
            } else {
                print("url != requested url, skipping")
            }
        }
    }
    
    func imageDownloadError(_ error: Error) {
        //something went wrong
        imageView.image = UIImage(named: "placeholder")
        print(error.localizedDescription)
    }
}
