//
//  Utils.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 23.10.2019.
//  Copyright © 2019 Alexey Taran. All rights reserved.
//

import Foundation

extension Array {
    func nextOrNil(for index: Int) -> Element? {
        return index + 1 == count ? nil : self[index + 1]
    }
    
    func previousOrNil(for index: Int) -> Element? {
        return index - 1 < 0 ? nil : self[index - 1]
    }
}
