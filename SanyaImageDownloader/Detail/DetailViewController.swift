//
//  DetailViewController.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 21.10.2019.
//  Copyright © 2019 Alexey Taran. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var detailedImageView: UIImageView!
    var detailedImageModel: ImageModel? {
        didSet {
            if detailedImageView != nil {
                loadImage()
            }
        }
    }
    private var zoomed = false

    override func viewDidLoad() {
        super.viewDidLoad()
        loadImage()
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tapRecognizer.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(tapRecognizer)
    }
    
    private func loadImage() {
        detailedImageView.image = nil
        if let detailedUrl = self.detailedImageModel?.imageUrl {
            DownloadUIManager.shared.downloadImage(from: detailedUrl) { [weak self] (url, image, error) in
                if error != nil {
                    self?.detailedImageView.image = UIImage(named: "placeholder")
                } else {
                    self?.detailedImageView.image = image
                    self?.enableZooming()
                }
            }
        } else {
            detailedImageView.image = UIImage(named: "placeholder")
        }
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            let tapPoint = sender.location(in: scrollView)
            if zoomed {
                let rectToZoom = CGRect(x: tapPoint.x, y: tapPoint.y, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                scrollView.zoom(to: rectToZoom, animated: true)
                zoomed = false
            } else {
                let rectToZoom = CGRect(x: tapPoint.x, y: tapPoint.y, width: scrollView.contentSize.width / 2, height: scrollView.contentSize.height / 2)
                scrollView.zoom(to: rectToZoom, animated: true)
                zoomed = true
            }
        }
    }
    
    private func enableZooming() {
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 3.0
        self.scrollView.contentSize = self.detailedImageView.frame.size
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.delegate = self
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        self.detailedImageView
    }
}
