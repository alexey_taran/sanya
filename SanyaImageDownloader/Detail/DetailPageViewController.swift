//
//  DetailPageViewController.swift
//  SanyaImageDownloader
//
//  Created by Alexey Taran on 23.10.2019.
//  Copyright © 2019 Alexey Taran. All rights reserved.
//

import UIKit

class DetailPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var initImageModel: ImageModel? = nil
    private var controllers = [DetailViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0...2 {
            controllers.append(initChildViewController())
        }
        dataSource = self
        let currentViewController = controllers[1]
        currentViewController.detailedImageModel = initImageModel
        setViewControllers([currentViewController], direction: .forward, animated: false, completion: nil)
        
    }
    
    private func initChildViewController() -> DetailViewController {
        return storyboard?.instantiateViewController(withIdentifier: "detailImageView") as! DetailViewController
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let detailViewController = viewController as! DetailViewController
        guard let currentImageModel = detailViewController.detailedImageModel else { return nil }
        let previousImageModel = DemoURL.getNextImageModel(for: currentImageModel, isNext: false)
        if previousImageModel != nil {
            let previousViewController = getNextViewController(for: detailViewController, isNext: false)
            previousViewController?.detailedImageModel = previousImageModel
            return previousViewController
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let detailViewController = viewController as! DetailViewController
        guard let currentImageModel = detailViewController.detailedImageModel else { return nil }
        let nextImageModel = DemoURL.getNextImageModel(for: currentImageModel, isNext: true)
        if nextImageModel != nil {
            let nextViewController = getNextViewController(for: detailViewController, isNext: true)
            nextViewController?.detailedImageModel = nextImageModel
            return nextViewController
        }
        return nil
    }
    
    private func getNextViewController(for viewController: DetailViewController, isNext: Bool) -> DetailViewController? {
        guard let indexOfCurrentViewController = controllers.firstIndex(of: viewController) else { return nil }
        if isNext {
            let nextController = controllers.nextOrNil(for: indexOfCurrentViewController)
            return nextController == nil ? controllers[0] : nextController
        } else {
            let previousController = controllers.previousOrNil(for: indexOfCurrentViewController)
            return previousController == nil ? controllers[2] : previousController
        }
    }
}
